//
//  SoteriaCrashReporter.h
//  SoteriaCrashReporter
//
//  Created by Navdeep Mahajan on 3/24/19.
//  Copyright © 2019 Soteria Analytics. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SoteriaCrashReporter.
FOUNDATION_EXPORT double SoteriaCrashReporterVersionNumber;

//! Project version string for SoteriaCrashReporter.
FOUNDATION_EXPORT const unsigned char SoteriaCrashReporterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SoteriaCrashReporter/PublicHeader.h>

#import <SoteriaCrashReporter/SoteriaCrashReportManager.h>
