//
//  SoteriaCrashReportManager.h
//  SoteriaCrashReporter
//
//  Created by Navdeep Mahajan on 3/24/19.
//  Copyright © 2019 Soteria Analytics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoteriaCrashReportManager : NSObject
typedef void (^SoteriaRemovePendingCrashReportCompletionBlock) (BOOL isSuccess);
typedef void (^SoteriaCrashReportReceivalBlock) (NSException* exception);

+(NSString *) formatNSException: (NSException *) exception;

+(void) registerSoteriaCrashReporterWithExceptionReceivalBlock:(SoteriaCrashReportReceivalBlock) receivalBlock;
@end
